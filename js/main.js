"use strict";

const tab = document.getElementsByClassName("tabs-title");
const tabContent = document.getElementsByClassName("tab-cont");

console.log(tab);
console.log(tabContent);
for (let index = 0; index < tab.length; index++){
    tab[index].addEventListener("click",function removeActive(){
        for (let index = 0; index < tab.length; index++){
            tab[index].classList.remove("active");
        };
        tab[index].classList.add("active");
        const tabActive = tab[index].getAttribute("data-tab-active");
        for (let index = 0; index < tabContent.length; index++){
            tabContent[index].classList.remove("content-active");
            for (let index = 0; index < tabContent.length; index++) {
                if(tabContent[index].classList.contains(`${tabActive}`)){
                    tabContent[index].classList.add("content-active");
                };
            }; 
        };
    });
};




